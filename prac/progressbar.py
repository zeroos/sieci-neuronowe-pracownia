import uuid
from IPython.display import HTML, Javascript, display


class Progressbar:

    def __init__(self):
        self.divid = str(uuid.uuid4())

        self.pb = HTML(
            """
            <div style="border: 1px solid black; width:500px">
            <div id="%s" style="background-color:blue; width:0%%">&nbsp;</div>
            </div>
            """ % self.divid)
        display(self.pb)

    def update(self, proc):
        display(Javascript("$('div#%s').width('%i%%')" % (self.divid, proc)))
