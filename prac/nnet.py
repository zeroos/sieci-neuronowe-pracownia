import numpy as np
import pandas
import os
import time
from matplotlib import pyplot

#################################################################
# Sprawdzanie gradientu
#################################################################

def grad(f, X, delta=1e-4):
    X = np.array(X)
    R = np.zeros_like(X)
    XF = X.ravel() #get the 1D views
    RF = R.ravel()
    for i in xrange(XF.shape[0]):
        xold = XF[i]
        XF[i] = xold+delta
        fp, unused_grad = f(X)
        XF[i] = xold-delta
        fn, unused_grad = f(X)
        XF[i] = xold
        RF[i] = (fp-fn)/(2*delta)
    return R

def check_grad(f, X, delta= 1e-4, prec=1e-6):
    fval, fgrad = f(X)
    num_grad = grad(f, X, delta=delta)
    diffnorm = np.sqrt(np.sum((fgrad-num_grad)**2))
    gradnorm = np.sqrt(np.sum(fgrad**2))
    return (diffnorm < prec or diffnorm/gradnorm < prec)

#################################################################
# Upychanie parametrow (wag i biasow) do wektora
#################################################################

def encodeParams(P):
    flat = np.hstack([p.ravel() for p in P])
    shapes = [p.shape for p in P]
    return flat, shapes

def decodeParams(P, shapes):
    ret = []
    i = 0
    for s in shapes:
        e = i + np.prod(s)
        ret.append(P[i:e].reshape(s))
        i = e
    return ret

#################################################################
# Prosta siec dwu-warstwowa
#################################################################

def simple_class_net(Params, X, Y=None):
    #
    # Note: this code assumes that each column of X is a sample!!!
    #
    W1,b1, W2,b2 = Params  #get the weights
    NS = X.shape[1]
    A1 = W1.dot(X) + b1    #.dot() is matrix multiply W1 * X. Bias is added to all samples using broadcasting
    H1 = np.tanh(A1)       #1st transfer fun
    A2 = W2.dot(H1) + b2
    O = A2 - A2.max(axis=0, keepdims=True) #Softmax, subtract max for stability
    O = np.exp(O) # elemwise exponentiation
    O = O / O.sum(axis=0) # elements of each row (axis 0) are divided by the row sum
    if Y is None:
        return O
    else:
        J = -1.0/NS * np.log(O[Y, range(NS)]).sum()  #this is an advanced indexing trick: http://docs.scipy.org/doc/numpy/reference/arrays.indexing.html#purely-integer-array-indexing
        dJdA2 = O
        dJdA2[Y, range(NS)] -= 1.0 #dLdA2_c = O_c - [Y=c]
        dJdA2 /= NS                #for normalization

        dJdH1 = W2.T.dot(dJdA2)
        dJdA1 = dJdH1 * (1 - H1**2) #tanh derivative

        dJdW2 = dJdA2.dot(H1.T)
        dJdb2 = dJdA2.sum(axis=1, keepdims=True)
        dJdW1 = dJdA1.dot(X.T)
        dJdb1 = dJdA1.sum(axis=1, keepdims=True)

        ########################################################
        #
        # Tu mozna dodac weight decay:
        #
        # J += ....
        # dJdW1 += ....
        # dJdW2 += ....
        #
        ########################################################

        dJdW2 += 100*W2

        return J, [dJdW1, dJdb1, dJdW2, dJdb2]

#################################################################
# Wyznaczanie poczatkowych wag i biasow
#################################################################

def init_net(n, nh, no):
    W1 = np.random.randn(nh,n)*0.02
    b1 = np.zeros((nh,1))
    W2 = np.random.randn(no,nh)*0.02
    b2 = np.zeros((no,1))
    return [W1,b1,W2,b2]

#################################################################
# Weryfikacja poprawnosci implementacji
#################################################################

def verify_network_function():
    import sklearn.datasets
    iris = sklearn.datasets.load_iris()

    trainX = iris.data.T
    trainY = iris.target

    Params = init_net(trainX.shape[0], 2, trainY.max()+1)

    Params_flat, Params_shape = encodeParams(Params)
    def f(Theta):
        P = decodeParams(Theta, Params_shape)
        J, dJdP = simple_class_net(P, trainX, trainY)
        dJdP_flat, unused_shapes = encodeParams(dJdP)
        return J, dJdP_flat

    return check_grad(f, Params_flat)

#################################################################
# Optymalizator SGD
#################################################################


stats = []
def SGD(Params, train_X, train_Y, test_X=None, test_Y=None,
        minibatch_size=8, alpha_0=1e-1, tau=10000, alpha_dec=None):
    NS = train_X.shape[1]
    iter = 0 #number od SGD steps
    epoch =0 #number of passes through the whole data
    max_iter= 1500
    best_test_err = np.inf
    epoch_start_time = time.time()

    global stats #ugly hack to keep stats even after run is interrupted
    stats = []

    previous_update = [np.zeros_like(p) for p in Params]
    while iter<max_iter:
        sample_indices = np.random.permutation(NS)
        minibatch_idxs = np.split(sample_indices, range(minibatch_size, NS, minibatch_size))
        for minibatch in minibatch_idxs:
            BX = train_X[:,minibatch]
            BY = train_Y[minibatch]

            iter += 1
            alpha = alpha_0 * tau / max(tau, iter)

            BJ, BdJdP = simple_class_net(Params, BX, BY)

            ######################################################
            #
            # Tu mozemy doimplementowac momentum
            #
            ######################################################

            for P, dP, pP in zip(Params, BdJdP, previous_update):
                P -= alpha * (dP + pP*(10**-15))
                pP = dP


            stats.append(dict(iter=iter, BJ=BJ, epoch=epoch, alpha=alpha))
            if iter%100 == 0:
                print 'Iter: %d, batch loss: %g, alpha:%g' %(iter, BJ, alpha)

        train_scores = simple_class_net(Params, train_X)
        train_preds = train_scores.argmax(axis=0)
        train_err = (train_Y != train_preds).mean()

        test_scores = simple_class_net(Params, test_X)
        test_preds = test_scores.argmax(axis=0)
        test_err = (test_Y != test_preds).mean()

        epoch_end_time = time.time()
        epoch +=1
        print 'Finished %d epochs. Last epoch took %f[s]' % (epoch, epoch_end_time-epoch_start_time)
        epoch_start_time = epoch_end_time

        if test_err < best_test_err:
            best_test_err = test_err
            max_iter = max(max_iter, 1.5*iter)
            print "Increasing iteration count to: %d" % (max_iter,)
        else:
            print "Still going to: %d iterations" % (max_iter,)
        print 'train_errs: %f, test_errs: %f' % (train_err, test_err)
        stats[-1]['train_err'] = train_err
        stats[-1]['test_err'] = test_err



def load_mnist():
    import scipy.io as sio

    if not os.path.exists('./mnist_all.mat'):
        import urllib
        print 'Downloading http://www.cs.nyu.edu/~roweis/data/mnist_all.mat'
        urllib.urlretrieve ("http://www.cs.nyu.edu/~roweis/data/mnist_all.mat", "./mnist_all.mat")

    mnist = sio.loadmat('./mnist_all.mat')


    train_X = []
    train_Y = []
    test_X = []
    test_Y = []
    for digit in xrange(10):
        train_X.append(mnist['train%d' % digit])
        train_Y.append(np.zeros((train_X[-1].shape[0],))+digit)

        test_X.append(mnist['test%d' % digit])
        test_Y.append(np.zeros((test_X[-1].shape[0],))+digit)

    train_X = (np.concatenate(train_X, axis=0)/255.).astype(np.float32)
    train_Y = np.concatenate(train_Y, axis=0).astype(np.int32)
    test_X = (np.concatenate(test_X, axis=0)/255.).astype(np.float32)
    test_Y = np.concatenate(test_Y, axis=0).astype(np.int32)

    return train_X.T, train_Y, test_X.T, test_Y

def plot_stats(stats):
    stats = pandas.DataFrame(stats)
    pyplot.semilogy(stats.iter, stats.BJ, alpha=0.2, label='minibatch loss')
    pyplot.ylabel('per batch loss')
    pyplot.xlabel('training iteration')
    epoch_stats = stats[np.isfinite(stats.train_err)]
    ax_err = pyplot.twinx(pyplot.gca())
    ax_err.plot(epoch_stats.iter, epoch_stats.train_err, label='train_err')
    ax_err.plot(epoch_stats.iter, epoch_stats.test_err, label='test_err')
    ax_err.set_ylabel('error rate')
    pyplot.legend()
    pyplot.show()

if __name__ == '__main__':
    if not verify_network_function():
        raise Exception("a")

    train_X, train_Y, test_X, test_Y = load_mnist()

    Params = init_net(train_X.shape[0], 150, train_Y.max()+1)

    try:
        SGD(Params, train_X, train_Y, test_X, test_Y)
    except KeyboardInterrupt:
        pass

    plot_stats(stats)





